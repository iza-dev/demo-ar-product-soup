import express from "express";
import path from "path";

const app = express();

app.use(express.static(path.resolve(process.cwd(), 'public')))

app.get('/', (_, res) => {
  res.sendFile(path.resolve(process.cwd(), 'public/index.html'))
})

const PORT = 3000;
app.listen(PORT, () => {
  console.log(`Server running at http://localhost:${PORT}/`);
});
