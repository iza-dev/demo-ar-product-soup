import * as THREE from 'three';

export function addLight(scene) {
    const sunLight = new THREE.DirectionalLight('#ffffff', 4);
    sunLight.castShadow = true;
    sunLight.shadow.camera.far = 15;
    sunLight.shadow.mapSize.set(1024, 1024);
    sunLight.shadow.normalBias = 0.05;
    sunLight.position.set(3.5, 2, -1.25);
    scene.add(sunLight);
}
