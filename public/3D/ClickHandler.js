import * as THREE from 'three';
import { onClick } from './Animations.js';

export async function handleClick(e, scene, camera) {
    const mouseX = (e.clientX / window.innerWidth) * 2 - 1;
    const mouseY = -1 * ((e.clientY / window.innerHeight) * 2 - 1);
    const mouse = new THREE.Vector2(mouseX, mouseY);
    const raycaster = new THREE.Raycaster();
    raycaster.setFromCamera(mouse, camera);
    const intersects = raycaster.intersectObjects(scene.children, true);
    if (intersects.length > 0) {
        let o = intersects[0].object;
        while (o.parent && !o.userData.clickable) {
            o = o.parent;
        }
        if (o.userData.clickable && o.name === "btn_action") {
            await onClick(o)
           
            window.location.href = "https://www.tanoshi.fr/tout-savoir-sur-le-miso/";
        }
    }
}
