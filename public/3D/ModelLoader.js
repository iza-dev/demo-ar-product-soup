import * as THREE from 'three';
import { GLTFLoader } from 'three/addons/gltfLoader';

export async function loadSoup(url, name, materials, scene, anchor) {
    const loader = new GLTFLoader();
    return new Promise((resolve, reject) => {
        loader.load(url, (gltf) => {
            gltf.scene.name = name;
            gltf.scene.scale.set(0.1, 0.1, 0.1);
            gltf.scene.position.set(0, -0.5, 0);
            gltf.scene.userData.clickable = true;
            gltf.scene.rotation.set(0.45, 0, 0);
            gltf.scene.traverse(child => {
                const material = materials[child.name];
                if (material) child.material = new THREE.MeshToonMaterial({ color: material });
                child.castShadow = true;
            });
            anchor.group.add(gltf.scene);
            resolve();
        }, undefined, reject);
    });
}

export async function loadCTA(url, name, anchor) {
    const loader = new GLTFLoader();
    return new Promise((resolve, reject) => {
        loader.load(url, (gltf) => {
            gltf.scene.name = name;
            gltf.scene.userData.clickable = true;

            const material = new THREE.MeshToonMaterial({ color: 0xD49B52 });
            gltf.scene.children[0].material = material;
            
            const texture = new THREE.TextureLoader().load('../../textures/UI/btn_action.png');
            texture.center = new THREE.Vector2(0.525, 0.375);
            texture.repeat.set( 5, 5 ); 
            texture.rotation = Math.PI / 2;
            texture.flipY = false;

            const material_btn_action = new THREE.MeshBasicMaterial({map: texture });
            gltf.scene.children[1].material = material_btn_action;

            gltf.scene.scale.set(0.15, 0.15, 0.15);
            gltf.scene.position.set(0, -0.75, 0);
            gltf.scene.children[1].userData.clickable = true;
            gltf.scene.children[1].name = name

            gltf.scene.children[0].userData.clickable = true;
            gltf.scene.children[0].name = name

            anchor.group.add(gltf.scene);
            resolve();
        }, undefined, reject);
    });
}

export async function loadBTNBubble( anchor) {
    const planeGeometry = new THREE.PlaneGeometry(0.5, 0.5);

    const texture_btn_1 = new THREE.TextureLoader().load('../../textures/UI/texture_1.png');
    const material_btn_1 = new THREE.MeshBasicMaterial({ map: texture_btn_1 });
    const plane_right = new THREE.Mesh(planeGeometry, material_btn_1);
    plane_right.position.set(0.5, 0.30, 0);
    plane_right.material.transparent = true;
    anchor.group.add(plane_right);

    const texture_btn_2 = new THREE.TextureLoader().load('../../textures/UI/texture_3.png');
    const material_btn_2 = new THREE.MeshBasicMaterial({ map: texture_btn_2 });
    const plane_left = new THREE.Mesh(planeGeometry, material_btn_2);
    plane_left.position.set(-0.55, 0.25, 0);
    plane_left.material.transparent = true;
    anchor.group.add(plane_left);

    const texture_btn_top = new THREE.TextureLoader().load('../../textures/UI/texture_2.png');
    const material_btn_top = new THREE.MeshBasicMaterial({ map: texture_btn_top });
    const plane_top = new THREE.Mesh(planeGeometry, material_btn_top);
    plane_top.position.set(0, 0.65, 0);
    plane_top.material.transparent = true;
    anchor.group.add(plane_top);

    return Promise.resolve();
}