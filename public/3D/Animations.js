export const onClick = async (model) => {
    return new Promise((resolve) => {
        // eslint-disable-next-line no-undef
        const TimeLine = new gsap.timeline();
        TimeLine.to(model.material.color, { duration: 0.5, r: 0.83, g: 0.61, b: 0.32 }).to(model.material.color, { duration: 0.5, r: 1, g: 1, b: 1 , onComplete: resolve });
    });
}
