import { MindARThree } from 'mindar-image-three';
import Time from './Utils/Time.js';
import { loadSoup, loadBTNBubble, loadCTA } from './ModelLoader.js';
import { handleClick } from './ClickHandler.js';
import { addLight } from './LightManager.js';

document.addEventListener('DOMContentLoaded', async () => {
    const mindarThree = new MindARThree({
        container: document.querySelector("#container"),
        imageTargetSrc: "data/product.mind"
    });

    const { renderer, scene, camera } = mindarThree;
    const anchor = mindarThree.addAnchor(0);

    const materials = {
        'bol': 0xA63744,
        'oignon': 0xA3D23B,
        'oignon001': 0x83AC15,
        'oignon002': 0x197E0B,
        'herbe': 0x197E0B,
        'champignons': 0x652A2A,
        'baguette': 0x1D1A19,
        'baguette1': 0x1D1A19,
        'soupe': 0xD49B52,
        'nouille1': 0xFFF1D8,
        'nouille2': 0xFFF1D8,
        'nouille3': 0xFFF1D8,
    };

    await loadSoup('models/soup.gltf', 'scoop', materials, scene, anchor);
    await loadCTA('models/btn_CTA.gltf', 'btn_action',  anchor);
    await loadBTNBubble(anchor);


    await mindarThree.start();  

    addLight(scene);

    document.body.addEventListener('click', (e) => {
        handleClick(e, scene, camera);
    });

    const time = new Time();
    time.on('tick', () => {
        renderer.render(scene, camera);
    });
});
